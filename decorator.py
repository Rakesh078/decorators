import time


def cal_square(numbers):
    """Calculate square of the number and return list and print time of executation.
    """
    start = time.time()
    result = []
    for num in numbers:
        result.append(num*num)
    end = time.time()
    print "cal_square take " + str((end - start) * 1000) + " ms"
    return result


def cal_cube(numbers):
    """
    Calculate cube of the number and return list and print time of executation.
    """
    start = time.time()
    result = []
    for num in numbers:
        result.append(num * num * num)
    end = time.time()
    print "cal_cube take " + str((end - start) * 1000) + " ms"
    return result

print "\n\n\n Printing without decorator"
print cal_square([1,2,3,4,5])
print cal_cube([1,2,3,4,5])

def time_it(func):
    """
    get time of excuation of the functions
    """
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print func.__name__ + " take " + str((end - start) * 1000) + " ms"
        return result
    return wrapper

@time_it
def cal_square2(numbers):
    result = []
    for num in numbers:
        result.append(num * num)
    return result

@time_it
@time_it
def cal_cube2(numbers):
    result = []
    for num in numbers:
        result.append(num * num * num)
    return result

print "\n\n\n Printing with decorator"
print cal_square2([1,2,3,4,5])
print cal_cube2([1,2,3,4,5])
